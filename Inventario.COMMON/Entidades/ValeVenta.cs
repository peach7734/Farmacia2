﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventario.COMMON.Entidades
{
    public class ValeVenta:Base
    {
        public string Cliente { get; set; }
        public string Empleado { get; set; }
        public string Folio { get; set; }
        public List<Ventas> Producto { get; set; }
        public float Total { get; set; }
        public string Fecha { get; set; }

    }
}
