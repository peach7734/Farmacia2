﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventario.COMMON.Entidades
{
    public class Ventas
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public float PrecioVenta { get; set; }
        public float Iva { get; set; }
        public float SubTotal { get; set; }
        public int Cantidad { get; set; }
        public float total { get; set; }
    }
}
