﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventario.COMMON.Entidades
{
    public class Empleados:Persona
    {
        public string Area { get; set; }
        public override string ToString()
        {
            return Nombre;
        }
    }
}
