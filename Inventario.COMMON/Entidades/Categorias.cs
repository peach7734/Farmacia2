﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventario.COMMON.Entidades
{
    public class Categorias:Base
    {
        public string Categoria { get; set; }
        public override string ToString()
        {
            return Categoria;
        }
    }
}
