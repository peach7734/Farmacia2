﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventario.COMMON.Entidades
{
    public class Persona:Base
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefon { get; set; }
        public string Correo { get; set; }

    }
}
