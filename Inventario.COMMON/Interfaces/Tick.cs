﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Inventario.COMMON.Interfaces
{
    public class Tick
    {
        public string TIKET { get; set; }
        public Tick(string tiket)
        {
            TIKET = tiket;
        }
        public bool GenerarTiket(string datos)
        {
            try
            {
                StreamWriter elementos = new StreamWriter(TIKET);
                elementos.Write(datos);
                elementos.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
