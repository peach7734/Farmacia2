﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.DAL
{
    public class RepositorioCliente : IRepositorio<Clientes>
    {
        private string DBName = "FarmaciaDelSol.db";
        private string TableName = "Clientes";
        public List<Clientes> Leer
        {
            get
            {
                List<Clientes> cliente = new List<Clientes>();
                using (var db = new LiteDatabase(DBName))
                {
                    cliente = db.GetCollection<Clientes>(TableName).FindAll().ToList();
                }
                return cliente;
            }
        }

        public bool Crear(Clientes entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Clientes>(TableName);
                    a.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(Clientes entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Clientes>(TableName);
                    a.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Clientes>(TableName);
                    r = a.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

