﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.DAL
{
    public class RepositorioProductos : IRepositorio<Productos>
    {

        private string DBName = "FarmaciaDelSol.db";
        private string TableName = "Productos";
        public List<Productos> Leer
        {
            get
            {
                List<Productos> producto = new List<Productos>();
                using (var db = new LiteDatabase(DBName))
                {
                    producto = db.GetCollection<Productos>(TableName).FindAll().ToList();
                }
                return producto;
            }
        }

        public bool Crear(Productos entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Productos>(TableName);
                    a.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(Productos entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Productos>(TableName);
                    a.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Productos>(TableName);
                    r = a.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

