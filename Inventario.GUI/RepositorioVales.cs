﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.DAL
{
    public class RepositorioVales : IRepositorio<ValeVenta>
    {
        private string DBName = "FarmaciaDelSol.db";
        private string TableName = "Vales";

        public List<ValeVenta> Leer
        {
            get
            {
                List<ValeVenta> vale = new List<ValeVenta>();
                using (var db = new LiteDatabase(DBName))
                {
                    vale = db.GetCollection<ValeVenta>(TableName).FindAll().ToList();
                }
                return vale;
            }
        }

        public bool Crear(ValeVenta entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<ValeVenta>(TableName);
                    a.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(ValeVenta entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<ValeVenta>(TableName);
                    a.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<ValeVenta>(TableName);
                    r = a.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
