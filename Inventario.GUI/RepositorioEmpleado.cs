﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.DAL
{
    public class RepositorioEmpleado : IRepositorio<Empleados>
    {
        private string DBName = "FarmaciaDelSol.db";
        private string TableName = "Empleados";
        public List<Empleados> Leer
        {
            get
            {
                List<Empleados> empleado = new List<Empleados>();
                using (var db = new LiteDatabase(DBName))
                {
                    empleado = db.GetCollection<Empleados>(TableName).FindAll().ToList();
                }
                return empleado;
            }
        }

        public bool Crear(Empleados entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Empleados>(TableName);
                    a.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(Empleados entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var a = db.GetCollection<Empleados>(TableName);
                    a.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Empleados>(TableName);
                    r = coleccion.Delete(e => e.Id == id); //expresion landa cambia deun foreach//especifique para encontar los elementos a eliminar
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
