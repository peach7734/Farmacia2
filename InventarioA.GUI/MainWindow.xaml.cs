﻿using Inventario.BIZ;
using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using Inventario.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InventarioA.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum Opcion
        {
            nuevo,
            edicion
        }
        Opcion operacion;
        IManejadorCategoria manejadorCategoria;

        IManejadorEmpleados manejadorEmpleados;
        IManejadorCliente manejadorCliente;
        IManejadorProducto manejadorProducto;
        IManejadorValeVenta manejadorValeVenta;

        List<Ventas> accion;
        public MainWindow()
        {
            InitializeComponent();
            manejadorCategoria = new ManejadorCategoria(new RepositorioCategoria());
            manejadorCliente = new ManejadorCliente(new RepositorioCliente());
            manejadorEmpleados = new ManejadorEmpleado(new RepositorioEmpleado());
            manejadorProducto = new ManejadorProducto(new RepositorioProductos());
            manejadorValeVenta = new ManejadorVale(new RepositorioVales());

            accion = new List<Ventas>();

            ActualizarTablas();
            HabilitarLimpiarCajas();
            HabilitarCajas(false);
            HabilitarCajasListas(false);

            CargarComboCategoria();

            HabilitarBotonesEnVenta(false);
            CargarTablaEnProcesoVenta();
            HabilitarCajasVenta(false);
        }

         private void HabilitarCajasVenta(bool v)
        {
            txtProductosVenta.IsEnabled = v;
            txtVentaCantidad.IsEnabled = v;
            txtFecha.IsEnabled = v;
            txtFolio.IsEnabled = v;
            cmbVentaCliente.IsEnabled = v;
            cmbVentaEmpleado.IsEnabled = v;
        }

        private void CargarComboCategoria() {
            cmbProductosCategoria.ItemsSource = null;
            cmbProductosCategoria.ItemsSource = manejadorCategoria.Lista;
        }

        private void CargarTablaEnProcesoVenta() {
            dtgVenta.ItemsSource = null;
            dtgVenta.ItemsSource = accion;
        }

        private void HabilitarCajasListas(bool v) {

            txtCategoria.IsEnabled = v;

            txtCLienteCorreo.IsEnabled = v;
            txtClienteDireccion.IsEnabled = v;
            txtClienteNombre.IsEnabled = v;
            txtClienteRFC.IsEnabled = v;
            txtClienteTelefono.IsEnabled = v;

            txtEmpleadoArea.IsEnabled = v;
            txtEmpleadoCorreo.IsEnabled = v;
            txtEmpleadoDireccion.IsEnabled = v;
            txtEmpleadoNombre.IsEnabled = v;
            txtEmpleadoTelefono.IsEnabled = v;

            txtPRoductosCOmpra.IsEnabled = v;
            txtProductosDescripcion.IsEnabled = v;
            txtProductosNombre.IsEnabled = v;
            txtProductosPResentacion.IsEnabled = v;
            txtProductosVenta.IsEnabled = v;
            cmbProductosCategoria.IsEnabled = v;
        }

        private void HabilitarBotonesEnVenta(bool v) {
            btnVentaAgregar.IsEnabled = v;
            btnVentaCancelar.IsEnabled = v;
            btnVentaEliminar.IsEnabled = v;
            btnVenta.IsEnabled = v;
            btnVentaNueva.IsEnabled = !v;
        }

        private void HabilitarEliminar(bool v) {
             btnVentaEliminar.IsEnabled = v;
        }

        private void HabilitarLimpiarCajas() {

            txtCategoria.Clear();

            txtCLienteCorreo.Clear();
            txtClienteDireccion.Clear();
            txtClienteNombre.Clear();
            txtClienteRFC.Clear();
            txtClienteTelefono.Clear();

            txtEmpleadoArea.Clear();
            txtEmpleadoCorreo.Clear();
            txtEmpleadoDireccion.Clear();
            txtEmpleadoNombre.Clear();
            txtEmpleadoTelefono.Clear();

            txtPRoductosCOmpra.Clear();
            txtProductosDescripcion.Clear();
            txtProductosNombre.Clear();
            txtProductosPResentacion.Clear();
            txtProductosVenta.Clear();
            
        }

        private void LimpiarCamposVenta() {
            txtVentaCantidad.Clear();
            dtgVenta.ItemsSource= null;
            accion = new List<Ventas>();
            cmbVentaCliente.ItemsSource = "";
            cmbVentaEmpleado.ItemsSource = "";

        }

        private void HabilitarCajas(bool v){
            btnCategoriaCancelar.IsEnabled = v;
            btnCategoriaEditar.IsEnabled = !v;
            btnCategoriaEliminar.IsEnabled = !v;
            btnCategoriaGuardar.IsEnabled = v;
            btnCategoriaNuevo.IsEnabled = !v;

            btnClienteCancelar.IsEnabled = v;
            btnClienteEditar.IsEnabled = !v;
            btnClienteEliminar.IsEnabled = !v;
            btnClienteGuardar.IsEnabled = v;
            btnClienteNuevo.IsEnabled = !v;

            btnEmpleadoCancelar.IsEnabled = v;
            btnEmpleadoEditar.IsEnabled = !v;
            btnEmpleadoEliminar.IsEnabled = !v;
            btnEmpleadoGuardar.IsEnabled = v;
            btnEmpleadoNuevo.IsEnabled = !v;

            btnProductosCancelar.IsEnabled = v;
            btnProductosditar.IsEnabled = !v;
            btnProductosEliminar.IsEnabled = !v;
            btnProductosGuardar.IsEnabled = v;
            btnProductosNuevo.IsEnabled = !v;
        }

        private void ActualizarTablas()
        {
            dtgCategoria.ItemsSource = null;
            dtgCategoria.ItemsSource = manejadorCategoria.Lista;

            dtgCliente.ItemsSource = null;
            dtgCliente.ItemsSource = manejadorCliente.Lista;

            dtgEmpleado.ItemsSource = null;
            dtgEmpleado.ItemsSource = manejadorEmpleados.Lista;

            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = manejadorProducto.Lista;

            dtgVentaProductos.ItemsSource = null;
            dtgVentaProductos.ItemsSource = manejadorProducto.Lista;

            cmbVentaCliente.ItemsSource = null;
            cmbVentaCliente.ItemsSource = manejadorCliente.Lista;

            cmbVentaEmpleado.ItemsSource = null;
            cmbVentaEmpleado.ItemsSource = manejadorEmpleados.Lista;

            dtgAlmacen.ItemsSource = null;
            dtgAlmacen.ItemsSource = manejadorValeVenta.Lista;
        }

        private void btnCategoriaGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtCategoria.Text)) {
                MessageBox.Show("No ha llenado todas las casillas", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (operacion == Opcion.nuevo)
            {
                Categorias a = new Categorias();
                a.Categoria = txtCategoria.Text;

                if (manejadorCategoria.Agregar(a))
                {
                    MessageBox.Show("Categoria Guardada", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
            else
            {
                Categorias a = dtgCategoria.SelectedItem as Categorias;
                a.Categoria = txtCategoria.Text;

                if (manejadorCategoria.Modificar(a))
                {
                    MessageBox.Show("Categoria Editada", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
        }

        private void btnClienteGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtClienteDireccion.Text)|| string.IsNullOrEmpty(txtCLienteCorreo.Text)|| string.IsNullOrEmpty(txtClienteNombre.Text) || string.IsNullOrEmpty(txtClienteRFC.Text) || string.IsNullOrEmpty(txtClienteTelefono.Text))
            {
                MessageBox.Show("No ha llenado todas las casillas", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (operacion == Opcion.nuevo)
            {
                Clientes a = new Clientes();
                a.Correo = txtCategoria.Text;
                a.Direccion = txtClienteDireccion.Text;
                a.Nombre = txtClienteNombre.Text;
                a.RFC = txtClienteRFC.Text;
                a.Telefon = txtClienteTelefono.Text;

                if (manejadorCliente.Agregar(a))
                {
                    MessageBox.Show("Cliente Guardado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
            else {
                Clientes a = dtgCliente.SelectedItem as Clientes;
                a.Correo = txtCategoria.Text;
                a.Direccion = txtClienteDireccion.Text;
                a.Nombre = txtClienteNombre.Text;
                a.RFC = txtClienteRFC.Text;
                a.Telefon = txtClienteTelefono.Text;

                if (manejadorCliente.Modificar(a))
                {
                    MessageBox.Show("Cliente Editado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
        }

        private void btnEmpleadoGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmpleadoArea.Text) || string.IsNullOrEmpty(txtEmpleadoCorreo.Text) || string.IsNullOrEmpty(txtEmpleadoDireccion.Text) || string.IsNullOrEmpty(txtEmpleadoNombre.Text) || string.IsNullOrEmpty(txtEmpleadoTelefono.Text))
            {
                MessageBox.Show("No ha llenado todas las casillas", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (operacion == Opcion.nuevo)
            {
                Empleados a = new Empleados();
                a.Area = txtEmpleadoArea.Text;
                a.Correo = txtEmpleadoCorreo.Text;
                a.Direccion = txtEmpleadoDireccion.Text;
                a.Nombre = txtEmpleadoNombre.Text;
                a.Telefon = txtEmpleadoTelefono.Text;

                if (manejadorEmpleados.Agregar(a))
                {
                    MessageBox.Show("Empleado Guardado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
            else {
                Empleados a = dtgEmpleado.SelectedItem as Empleados;
                a.Area = txtEmpleadoArea.Text;
                a.Correo = txtEmpleadoCorreo.Text;
                a.Direccion = txtEmpleadoDireccion.Text;
                a.Nombre = txtEmpleadoNombre.Text;
                a.Telefon = txtEmpleadoTelefono.Text;

                if (manejadorEmpleados.Modificar(a))
                {
                    MessageBox.Show("Empleado Modificado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
        }

        private void btnProductosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPRoductosCOmpra.Text) || string.IsNullOrEmpty(txtProductosDescripcion.Text) || string.IsNullOrEmpty(txtProductosNombre.Text) || string.IsNullOrEmpty(txtProductosPResentacion.Text) || string.IsNullOrEmpty(txtProductosVenta.Text))
            {
                MessageBox.Show("No ha llenado todas las casillas", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            foreach (var item in txtPRoductosCOmpra.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números , no caracteres en precio compra", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            foreach (var item in txtProductosVenta.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números , no caracteres en precio venta", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (operacion == Opcion.nuevo)
            {
                Productos a = new Productos();
                a.Descripcion = txtProductosDescripcion.Text;
                a.Nombre = txtProductosNombre.Text;
                a.PrecioCompra = txtPRoductosCOmpra.Text;
                a.PrecioVenta = txtProductosVenta.Text;
                a.Presentacion = txtProductosPResentacion.Text;
                a.Categoria = cmbProductosCategoria.Text;
                if (manejadorProducto.Agregar(a))
                {
                    MessageBox.Show("Productos Guardados", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
            else {
                Productos a = dtgProductos.SelectedItem as Productos;
                a.Descripcion = txtProductosDescripcion.Text;
                a.Nombre = txtProductosNombre.Text;
                a.PrecioCompra = txtPRoductosCOmpra.Text;
                a.PrecioVenta = txtProductosVenta.Text;
                a.Presentacion = txtProductosPResentacion.Text;
                a.Categoria = cmbProductosCategoria.Text;
                if (manejadorProducto.Modificar(a))
                {
                    MessageBox.Show("Productos Editado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablas();
                    HabilitarLimpiarCajas();
                    HabilitarCajas(false);
                    HabilitarCajasListas(false);
                }
            }
        }

        private void btnCategoriaNuevo_Click(object sender, RoutedEventArgs e)
        {
            operacion = Opcion.nuevo;
            HabilitarLimpiarCajas();
            HabilitarCajas(true);
            HabilitarCajasListas(true);
        }

        private void btnClienteNuevo_Click(object sender, RoutedEventArgs e)
        {
            operacion = Opcion.nuevo;
            HabilitarLimpiarCajas();
            HabilitarCajas(true);
            HabilitarCajasListas(true);
        }

        private void btnEmpleadoNuevo_Click(object sender, RoutedEventArgs e)
        {
            operacion = Opcion.nuevo;
            HabilitarLimpiarCajas();
            HabilitarCajas(true);
            HabilitarCajasListas(true);
        }

        private void btnProductosNuevo_Click(object sender, RoutedEventArgs e)
        {
            operacion = Opcion.nuevo;
            HabilitarLimpiarCajas();
            HabilitarCajas(true);
            HabilitarCajasListas(true);
        }

        private void btnProductosCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta realmente seguro de cancelar?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) { 
                HabilitarLimpiarCajas();
                HabilitarCajas(false);
                HabilitarCajasListas(false);
            }
        }

        private void btnEmpleadoCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta realmente seguro de cancelar?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                HabilitarLimpiarCajas();
                HabilitarCajas(false);
                HabilitarCajasListas(false);
            }
        }

        private void btnClienteCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta realmente seguro de cancelar?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                HabilitarLimpiarCajas();
                HabilitarCajas(false);
                HabilitarCajasListas(false);
            }
        }

        private void btnCategoriaCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta realmente seguro de cancelar?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                HabilitarLimpiarCajas();
                HabilitarCajas(false);
                HabilitarCajasListas(false);
            }
        }

        private void btnCategoriaEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgCategoria.SelectedItem != null)
            {
                    Categorias a = dtgCategoria.SelectedItem as Categorias;
                    HabilitarLimpiarCajas();
                     HabilitarCajasListas(true);
                     txtCategoria.Text = a.Categoria;
                    operacion = Opcion.edicion;
                    HabilitarCajas(true);
            }            
            else
            {
                MessageBox.Show("No ha seleccionado ninguna Categoria", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnClienteEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgCliente.SelectedItem != null)
            {
                Clientes a = dtgCliente.SelectedItem as Clientes;
                HabilitarLimpiarCajas();
                HabilitarCajasListas(true);
                txtCLienteCorreo.Text = a.Correo;
                txtClienteDireccion.Text = a.Direccion;
                txtClienteNombre.Text = a.Nombre;
                txtClienteRFC.Text = a.RFC;
                txtClienteTelefono.Text = a.Telefon;
                operacion = Opcion.edicion;
                HabilitarCajas(true);
            }
            else
            {
                MessageBox.Show("No ha seleccionado ninguna Cliente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnEmpleadoEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgEmpleado.SelectedItem != null)
            {
                Empleados a = dtgEmpleado.SelectedItem as Empleados;
                HabilitarLimpiarCajas();
                HabilitarCajasListas(true);
                txtEmpleadoArea.Text = a.Area;
                txtEmpleadoCorreo.Text = a.Correo;
                txtEmpleadoDireccion.Text = a.Direccion;
                txtEmpleadoNombre.Text = a.Nombre;
                txtEmpleadoTelefono.Text = a.Telefon;
                operacion = Opcion.edicion;
                HabilitarCajas(true);
            }
            else
            {
                MessageBox.Show("No ha seleccionado ninguna Empleado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnProductosditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgProductos.SelectedItem != null)
            {
                Productos a = dtgProductos.SelectedItem as Productos;
                HabilitarLimpiarCajas();
                HabilitarCajasListas(true);
                txtPRoductosCOmpra.Text = a.PrecioCompra;
                txtProductosDescripcion.Text = a.Descripcion;
                txtProductosNombre.Text = a.Nombre;
                txtProductosPResentacion.Text = a.Presentacion;
                txtProductosVenta.Text = a.PrecioVenta;
                cmbProductosCategoria.Text = a.Categoria;
                operacion = Opcion.edicion;
                HabilitarCajas(true);
            }
            else
            {
                MessageBox.Show("No ha seleccionado ninguna Cliente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnProductosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Productos a = dtgProductos.SelectedItem as Productos;
            if (a != null)
            {
                if (MessageBox.Show("Realmente esta seguro de eliminar el producto: "+a.Nombre ,"Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    if (manejadorProducto.Eliminar(a.Id))
                    {
                        MessageBox.Show("Producto eliminado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablas();
                       // ActualizarComboxDeTodosLosFormularios();
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun producto", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnEmpleadoEliminar_Click(object sender, RoutedEventArgs e)
        {
            Empleados a = dtgEmpleado.SelectedItem as Empleados;
            if (a != null)
            {
                if (MessageBox.Show("Realmente seguro de dar de baja al Empleado: " + a.Nombre, "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    if (manejadorEmpleados.Eliminar(a.Id))
                    {
                        MessageBox.Show("Empleado dado de baja", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablas();
                        // ActualizarComboxDeTodosLosFormularios();
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun Empleado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnClienteEliminar_Click(object sender, RoutedEventArgs e)
        {
            Clientes a = dtgCliente.SelectedItem as Clientes;
            if (a != null)
            {
                if (MessageBox.Show("Realmente esta seguro de dar de baja al Cliente: " + a.Nombre, "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    if (manejadorCliente.Eliminar(a.Id))
                    {
                        MessageBox.Show("Cliente dado de baja", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablas();
                        // ActualizarComboxDeTodosLosFormularios();
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun Cliente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnCategoriaEliminar_Click(object sender, RoutedEventArgs e)
        {
            Categorias a = dtgCategoria.SelectedItem as Categorias;
            if (a != null)
            {
                if (MessageBox.Show("Realmente esta seguro dar de baja a la categoria: " + a.Categoria, "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    if (manejadorCategoria.Eliminar(a.Id))
                    {
                        MessageBox.Show("Categoria dada de baja", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablas();
                        // ActualizarComboxDeTodosLosFormularios();
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ninguna categoria", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnVentaAgregar_Click(object sender, RoutedEventArgs e)
        {

            Productos a = dtgVentaProductos.SelectedItem as Productos;
            if (a != null)
            {
                if (string.IsNullOrEmpty(txtVentaCantidad.Text)) {
                    MessageBox.Show("No ha llenado el campo de cantidad :(","Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                foreach (var item in txtVentaCantidad.Text)
                {
                    if (!(char.IsNumber(item)))
                    {
                        MessageBox.Show("Solo se permiten números, no caracteres en cantidad de productos", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }

                float ivas = 0.16f;
                Ventas s = new Ventas();
                
                s.Cantidad = int.Parse(txtVentaCantidad.Text);
                s.Descripcion = a.Descripcion;
                s.Nombre = a.Nombre;
                s.PrecioVenta = float.Parse(a.PrecioVenta);
                s.Iva = ivas * s.PrecioVenta;
                s.SubTotal = s.Iva + s.PrecioVenta;
                s.total = s.SubTotal * s.Cantidad;
                accion.Add(s);
                CargarTablaEnProcesoVenta();
                txtVentaCantidad.Clear();
            }
            else {
                MessageBox.Show("No ha seleccionado ningun producto para venta", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnVenta_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbVentaCliente.Text)|| string.IsNullOrEmpty(cmbVentaEmpleado.Text)|| string.IsNullOrEmpty(txtFolio.Text) || string.IsNullOrEmpty(txtFecha.Text)) {
                MessageBox.Show("No ha llenado los datos del empleado - cliente - Fecha");
                return;
            }
            if (accion.Count==0) {
                MessageBox.Show("No hay datos para venta", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            float final = 0;
            foreach (Ventas item in accion)
            {
                final+= item.total;
            }
            Tick coleccion = new Tick(txtFolio.Text+".oh");
            string a1 = "", a2="", a3="";
            a1 = string.Format("Farmacia El Sol\nHuichapan\nNumeros 55 97 85 57 75 55\nCliente {0}\nEmpleado {1}\n\nNombre Descripcion Cantidad Precio Cantidad Iva Total", cmbVentaCliente.Text, cmbVentaEmpleado.Text);
            foreach (Ventas item in accion)
            {
                a2 += string.Format("{0}     {1}  {2}     {3}     {4}     {5}    {6}    {7}\n", item.Nombre, item.Descripcion, item.Cantidad, item.PrecioVenta, item.Cantidad, item.SubTotal, item.Iva, item.total);
            }
            a3 = string.Format("\nTotal ${0}", final);
           coleccion.GenerarTiket(a1 + a2+ a3);
            MessageBox.Show("El Total de la venta es: "+final+"\nTiket Guardado con el nombre de : " +txtFolio.Text,"Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);

            //LLenar datos en la tabla de Almacen
            ValeVenta a = new ValeVenta() {
                Cliente = cmbVentaCliente.Text,
                Empleado = cmbVentaEmpleado.Text,
                Fecha = txtFecha.Text,
                Producto = accion,
                Total = final,
                Folio= txtFolio.Text,
            };
            manejadorValeVenta.Agregar(a);        

            txtFolio.Clear();
            LimpiarCamposVenta();
            HabilitarBotonesEnVenta(false);
            ActualizarTablas();
            CargarTablaEnProcesoVenta();
            HabilitarCajasVenta(false);
        }

        private void btnVentaNueva_Click(object sender, RoutedEventArgs e)
        {
            Random a = new Random();
            int folio= a.Next(0, 999999999);
            txtFolio.Text = folio.ToString();
            LimpiarCamposVenta();
            HabilitarBotonesEnVenta(true);
            ActualizarTablas();
            CargarTablaEnProcesoVenta();
            HabilitarCajas(true);
            HabilitarCajasVenta(true);
        }

        private void btnVentaCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Seguro de cancelar la venta", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
                LimpiarCamposVenta();
                HabilitarBotonesEnVenta(false);
                txtFolio.Clear();
                HabilitarCajas(false);
                HabilitarCajasVenta(false);
            }
        }

        private void btnVentaEliminar_Click(object sender, RoutedEventArgs e)
        {

            if (dtgVenta.SelectedItem != null || dtgAlmacen.SelectedItem != null)
            {
                if (dtgVenta.SelectedItem != null)
                {
                    Ventas a = dtgVenta.SelectedItem as Ventas;
                    if (a != null)
                    {
                        if (MessageBox.Show("Seguro de eliminar este campo", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                        {
                            accion.Remove(a);
                            CargarTablaEnProcesoVenta();
                        }
                    }

                }
                else
                {
                    if (dtgAlmacen.SelectedItem != null)
                    {
                        ValeVenta b = dtgAlmacen.SelectedItem as ValeVenta;
                        if (MessageBox.Show("Seguro de eliminar este campo", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                        {
                            manejadorValeVenta.Eliminar(b.Id);
                            CargarTablaEnProcesoVenta();
                            ActualizarTablas();
                            HabilitarEliminar(false);
                        }
                    }
                }
            }
            else {
                MessageBox.Show("No ha seleccionado ningua fila de la tabla de Inventario o almacen", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }      
        }

        private void ObservarAlmacen() {
            ValeVenta a = dtgAlmacen.SelectedItem as ValeVenta;
            cmbVentaCliente.Text = a.Cliente;
            cmbVentaEmpleado.Text = a.Empleado;
            dtgVenta.ItemsSource = null;
            dtgVenta.ItemsSource = a.Producto;
            txtFecha.Text = a.Fecha;
            txtFolio.Text = a.Folio;
            MessageBox.Show("El total Fue de: $"+ a.Total, "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        

        private void dtgAlmacen_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            HabilitarEliminar(true);
            if (accion.Count > 0)
            {
                if (MessageBox.Show("Realmente quiere ver el Almacen", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    ObservarAlmacen();
                    HabilitarCajasVenta(true);
                }
                else
                {
                    return;
                }
            }
            else
            {
                ObservarAlmacen();
                HabilitarCajasVenta(true);
            }
        }
    }
}
