﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.BIZ
{
    public class ManejadorEmpleado : IManejadorEmpleados
    {
        IRepositorio<Empleados> empleado;
        public ManejadorEmpleado(IRepositorio<Empleados> empleado)
        {
            this.empleado = empleado;
        }

        public List<Empleados> Lista => empleado.Leer;

        public bool Agregar(Empleados entidad)
        {
            return empleado.Crear(entidad);
        }

        public Empleados BuscarIdentificador(string id)
        {
            return Lista.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return empleado.Eliminar(id);
        }

        public bool Modificar(Empleados entidad)
        {
            return empleado.Editar(entidad);
        }
    }
}
