﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.BIZ
{
    public class ManejadorCategoria : IManejadorCategoria
    {
        IRepositorio<Categorias> categoria;
        public ManejadorCategoria(IRepositorio<Categorias> categoria)
        {
            this.categoria = categoria;
        }

        public List<Categorias> Lista => categoria.Leer;

        public bool Agregar(Categorias entidad)
        {
            return categoria.Crear(entidad);
        }

        public Categorias BuscarIdentificador(string id)
        {
            return Lista.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return categoria.Eliminar(id);
        }

        public bool Modificar(Categorias entidad)
        {
            return categoria.Editar(entidad);
        }
    }
}
