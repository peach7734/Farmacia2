﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.BIZ
{
    public class ManejadorCliente : IManejadorCliente
    {
        IRepositorio<Clientes> cliente;
        public ManejadorCliente(IRepositorio<Clientes> cliente)
        {
            this.cliente = cliente;
        }
        public List<Clientes> Lista => cliente.Leer;

        public bool Agregar(Clientes entidad)
        {
            return cliente.Crear(entidad);
        }

        public Clientes BuscarIdentificador(string id)
        {
            return Lista.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return cliente.Eliminar(id);
        }

        public bool Modificar(Clientes entidad)
        {
            return cliente.Editar(entidad);
        }
    }
}
