﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.BIZ
{
    public class ManejadorProducto : IManejadorProducto
    {
        IRepositorio<Productos> producto;
        public ManejadorProducto(IRepositorio<Productos> producto)
        {
            this.producto = producto;
        }
        public List<Productos> Lista => producto.Leer;

        public bool Agregar(Productos entidad)
        {
            return producto.Crear(entidad);
        }

        public Productos BuscarIdentificador(string id)
        {
            return Lista.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return producto.Eliminar(id);
        }

        public bool Modificar(Productos entidad)
        {
            return producto.Editar(entidad);
        }
    }
}
