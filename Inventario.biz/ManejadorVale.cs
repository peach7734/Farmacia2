﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventario.BIZ
{
    public class ManejadorVale : IManejadorValeVenta
    {
        IRepositorio<ValeVenta> vale;
        public ManejadorVale(IRepositorio<ValeVenta> vale)
        {
            this.vale = vale;
        }
        public List<ValeVenta> Lista => vale.Leer;

        public bool Agregar(ValeVenta entidad)
        {
            return vale.Crear(entidad);
        }

        public ValeVenta BuscarIdentificador(string id)
        {
            return Lista.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return vale.Eliminar(id);
        }

        public bool Modificar(ValeVenta entidad)
        {
            return vale.Editar(entidad);
        }
    }
}
